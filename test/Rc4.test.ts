import { Rc4, resetState, StateHolder, swapBytes } from '../src/Rc4';

test('Swap bytes', () => {
  const data = new Uint8Array([1, 2, 3]);
  swapBytes(data, 0, 1);

  expect(data[0]).toBe(2);
  expect(data[1]).toBe(1);
  expect(data[2]).toBe(3);

  swapBytes(data, 1, 2);

  expect(data[0]).toBe(2);
  expect(data[1]).toBe(3);
  expect(data[2]).toBe(1);
});

test('Reset state', () => {
  const holder: StateHolder = {
    state: new Uint8Array(256),
    x: 0,
    y: 0,
  };

  expect(holder.state).toEqual(new Uint8Array(256));

  resetState(holder);
  for (let i = 0; i < 256; i++) {
    expect(holder.state[i]).toBe(i);
  }
});

test('Decrypt known ciphertext', () => {
  const cipher = new Rc4(new Uint8Array([42, 42, 42, 42]));
  const ciphertext = cipher.crypt(new Uint8Array([1, 2, 3, 4, 5, 6, 7, 8]));

  expect(ciphertext).toEqual(new Uint8Array([214, 16, 125, 251, 207, 23, 50, 150]));
});
