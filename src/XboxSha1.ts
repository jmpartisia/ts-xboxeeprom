import { XboxVersion } from './Types';

const SHA1_HASH_SIZE = 20;
const MASK_32 = 0xffffffff;

export interface Context {
  readonly intermediateHash: Uint32Array;

  lengthLow: number;
  lengthHigh: number;

  messageBlockIndex: number;
  readonly messageBlocks: Uint32Array;

  computed: Result;
  corrupted: Result;
}

export enum Result {
  SHA_SUCCESS = 0,
  SHA_NULL = 1,
  SHA_STATE_ERROR = 2,
}

/**
 * This is a TypeScript port of the SHA1 implementation found in ConfigMagic.
 *
 * The EEPROM HMAC and decryption process relies on some manipulation of the internal state of the SHA1.
 */
export class XboxSha1 {
  public readonly context: Context;

  constructor() {
    this.context = {
      intermediateHash: new Uint32Array(5),
      lengthLow: 0,
      lengthHigh: 0,
      messageBlockIndex: 0,
      messageBlocks: new Uint32Array(64),
      computed: Result.SHA_SUCCESS,
      corrupted: Result.SHA_SUCCESS,
    };

    this.reset();
  }

  public digest(): Uint32Array {
    const result = new Uint32Array(SHA1_HASH_SIZE);
    const context = this.context;

    if (context.corrupted === Result.SHA_NULL) {
      return result;
    }

    if (context.computed === Result.SHA_SUCCESS) {
      this.padMessage();

      for (let i = 0; i < 64; ++i) {
        context.messageBlocks[i] = 0;
      }
      context.lengthLow = 0;
      context.lengthHigh = 0;
      context.computed = Result.SHA_NULL;
    }

    for (let i = 0; i < SHA1_HASH_SIZE; ++i) {
      const index = i >> 2;
      const intermediate = context.intermediateHash[index];
      result[i] = intermediate >> (8 * (3 - (i & 0x03)));
    }
    return result;
  }

  /**
   * Reset the SHA1 state during HMAC with the "Friday 13th" middle message hack values.
   * 
   * @param version the xbox version to reset the SHA1 context with.
   */
  public hmac1reset(version: XboxVersion) {
    this.reset();

    const intermediate = this.context.intermediateHash;
    switch (version) {
      case XboxVersion.NONE:
        intermediate[0] = 0x85f9e51a;
        intermediate[1] = 0xe04613d2;
        intermediate[2] = 0x6d86a50c;
        intermediate[3] = 0x77c32e3c;
        intermediate[4] = 0x4bd717a4;
        break;
      case XboxVersion.V1_0:
        intermediate[0] = 0x72127625;
        intermediate[1] = 0x336472b9;
        intermediate[2] = 0xbe609bea;
        intermediate[3] = 0xf55e226b;
        intermediate[4] = 0x99958dac;
        break;
      case XboxVersion.V1_1:
        intermediate[0] = 0x39b06e79;
        intermediate[1] = 0xc9bd25e8;
        intermediate[2] = 0xdbc6b498;
        intermediate[3] = 0x40b4389d;
        intermediate[4] = 0x86bbd7ed;
        break;
      case XboxVersion.V1_2:
        intermediate[0] = 0x8058763a;
        intermediate[1] = 0xf97d4e0e;
        intermediate[2] = 0x865a9762;
        intermediate[3] = 0x8a3d920d;
        intermediate[4] = 0x08995b2c;
        break;
    }

    this.context.lengthLow = 512;
  }

  /**
   * Reset the SHA1 state during HMAC with the "Friday 13th" middle message hack values.
   * 
   * @param version the xbox version to reset the SHA1 context with.
   */
  public hmac2reset(version: XboxVersion) {
    this.reset();

    const intermediate = this.context.intermediateHash;
    switch (version) {
      case XboxVersion.NONE:
        intermediate[0] = 0x5d7a9c6b;
        intermediate[1] = 0xe1922beb;
        intermediate[2] = 0xb82ccdbc;
        intermediate[3] = 0x3137ab34;
        intermediate[4] = 0x486b52b3;
        break;
      case XboxVersion.V1_0:
        intermediate[0] = 0x76441d41;
        intermediate[1] = 0x4de82659;
        intermediate[2] = 0x2e8ef85e;
        intermediate[3] = 0xb256faca;
        intermediate[4] = 0xc4fe2de8;
        break;
      case XboxVersion.V1_1:
        intermediate[0] = 0x9b49bed3;
        intermediate[1] = 0x84b430fc;
        intermediate[2] = 0x6b8749cd;
        intermediate[3] = 0xebfe5fe5;
        intermediate[4] = 0xd96e7393;
        break;
      case XboxVersion.V1_2:
        intermediate[0] = 0x01075307;
        intermediate[1] = 0xa2f1e037;
        intermediate[2] = 0x1186eeea;
        intermediate[3] = 0x88da9992;
        intermediate[4] = 0x168a5609;
        break;
    }

    this.context.lengthLow = 512;
  }

  public reset(): Result {
    const context = this.context;

    context.lengthLow = 0;
    context.lengthHigh = 0;
    context.messageBlockIndex = 0;

    context.intermediateHash[0] = 0x67452301;
    context.intermediateHash[1] = 0xefcdab89;
    context.intermediateHash[2] = 0x98badcfe;
    context.intermediateHash[3] = 0x10325476;
    context.intermediateHash[4] = 0xc3d2e1f0;

    context.computed = Result.SHA_SUCCESS;
    context.corrupted = Result.SHA_SUCCESS;

    return Result.SHA_SUCCESS;
  }

  public input(message: Uint8Array, length: number): Result {
    const context = this.context;

    if (context.computed !== 0) {
      context.corrupted = Result.SHA_NULL;
      return Result.SHA_STATE_ERROR;
    }

    if (context.corrupted !== 0) {
      return context.corrupted;
    }

    for (let messageIndex = 0; messageIndex < length; messageIndex++) {
      context.messageBlocks[context.messageBlockIndex++] = message[messageIndex];

      context.lengthLow += 8;
      context.lengthLow = context.lengthLow & MASK_32;
      if (context.lengthLow === 0) {
        context.lengthHigh++;
        context.lengthHigh = context.lengthHigh & MASK_32;
        if (context.lengthHigh === 0) {
          /* Message is too long */
          context.corrupted = Result.SHA_NULL;
        }
      }

      if (context.messageBlockIndex === 64) {
        this.processMessageBlock();
      }

      if (context.corrupted !== 0) {
        break;
      }
    }

    return Result.SHA_SUCCESS;
  }

  private processMessageBlock(): void {
    const context = this.context;

    const K = new Uint32Array([0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6]);

    const W = new Uint32Array(80);
    let temp: number;

    let A: number;
    let B: number;
    let C: number;
    let D: number;
    let E: number;

    /*
     *  Initialize the first 16 words in the array W
     */
    for (let t = 0; t < 16; t++) {
      W[t] = context.messageBlocks[t * 4] << 24;
      W[t] |= context.messageBlocks[t * 4 + 1] << 16;
      W[t] |= context.messageBlocks[t * 4 + 2] << 8;
      W[t] |= context.messageBlocks[t * 4 + 3];
    }

    for (let t = 16; t < 80; t++) {
      W[t] = this.circularShift(1, W[t - 3] ^ W[t - 8] ^ W[t - 14] ^ W[t - 16]);
    }

    A = context.intermediateHash[0];
    B = context.intermediateHash[1];
    C = context.intermediateHash[2];
    D = context.intermediateHash[3];
    E = context.intermediateHash[4];

    for (let t = 0; t < 20; t++) {
      temp = this.circularShift(5, A) + ((B & C) | (~B & D)) + E + W[t] + K[0];
      E = D;
      D = C;
      C = this.circularShift(30, B);

      B = A;
      A = temp;
    }

    for (let t = 20; t < 40; t++) {
      temp = this.circularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[1];

      E = D;
      D = C;
      C = this.circularShift(30, B);
      B = A;
      A = temp;
    }

    for (let t = 40; t < 60; t++) {
      temp = this.circularShift(5, A) + ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
      E = D;
      D = C;
      C = this.circularShift(30, B);
      B = A;
      A = temp;
    }

    for (let t = 60; t < 80; t++) {
      temp = this.circularShift(5, A) + (B ^ C ^ D) + E + W[t] + K[3];
      E = D;
      D = C;
      C = this.circularShift(30, B);
      B = A;
      A = temp;
    }

    context.intermediateHash[0] += A;
    context.intermediateHash[1] += B;
    context.intermediateHash[2] += C;
    context.intermediateHash[3] += D;
    context.intermediateHash[4] += E;

    context.messageBlockIndex = 0;
  }

  private circularShift(bits: number, word: number) {
    const word32 = word & MASK_32;

    const left = word32 << bits;
    const right = word32 >>> (32 - bits);

    return (left & MASK_32) | (right & MASK_32);
  }

  private padMessage(): void {
    const context = this.context;

    if (context.messageBlockIndex > 55) {
      context.messageBlocks[context.messageBlockIndex++] = 0x80;
      while (context.messageBlockIndex < 64) {
        context.messageBlocks[context.messageBlockIndex++] = 0;
      }

      this.processMessageBlock();

      while (context.messageBlockIndex < 56) {
        context.messageBlocks[context.messageBlockIndex++] = 0;
      }
    } else {
      context.messageBlocks[context.messageBlockIndex++] = 0x80;
      while (context.messageBlockIndex < 56) {
        context.messageBlocks[context.messageBlockIndex++] = 0;
      }
    }

    /*
     *  Store the message length as the last 8 octets
     */
    context.messageBlocks[56] = context.lengthHigh >> 24;
    context.messageBlocks[57] = context.lengthHigh >> 16;
    context.messageBlocks[58] = context.lengthHigh >> 8;
    context.messageBlocks[59] = context.lengthHigh;
    context.messageBlocks[60] = context.lengthLow >> 24;
    context.messageBlocks[61] = context.lengthLow >> 16;
    context.messageBlocks[62] = context.lengthLow >> 8;
    context.messageBlocks[63] = context.lengthLow;

    this.processMessageBlock();
  }
}
