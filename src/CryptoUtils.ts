import { XboxVersion } from './Types';
import { XboxSha1 } from './XboxSha1';

export const calculateHddPassword = (key: Uint8Array, model: Uint8Array, serialNumber: Uint8Array): Uint8Array => {
  const state1 = new Uint8Array(0x40);
  const state2 = new Uint8Array(0x40 + 0x14);

  for (let i = 0x40 - 1; i >= key.length; --i) {
    state1[i] = 0x36;
  }

  for (let i = key.length - 1; i >= 0; --i) {
    state1[i] = (key[i] & 0xff) ^ 0x36;
  }

  const sha1 = new XboxSha1();
  sha1.reset();
  sha1.input(state1, 0x40);
  sha1.input(model, model.length);
  sha1.input(serialNumber, serialNumber.length);

  const digest = sha1.digest();
  for (let i = 0; i < 0x14; i++) {
    state2[0x40 + i] = digest[i] & 0xff;
  }

  for (let i = 0x40 - 1; i >= key.length; --i) {
    state2[i] = 0x5c;
  }
  for (let i = key.length - 1; i >= 0; --i) {
    state2[i] = (key[i] & 0xff) ^ 0x5c;
  }

  sha1.reset();
  sha1.input(state2, 0x40 + 0x14);
  return new Uint8Array(sha1.digest());
};

export const xboxHmacSha1 = (version: XboxVersion, messages: Uint8Array[]): Uint8Array => {
  const sha1 = new XboxSha1();
  sha1.hmac1reset(version);

  for (const message of messages) {
    sha1.input(message, message.length);
  }

  const subResult = sha1.digest();
  for (let i = 0; i < subResult.length; i++) {
    const x = subResult[i];
    sha1.context.messageBlocks[i] = x;
  }

  sha1.reset();
  sha1.hmac2reset(version);
  sha1.input(new Uint8Array(sha1.context.messageBlocks), 0x14);

  return new Uint8Array(sha1.digest());
};
