# ts-xboxeeprom 

A TypeScript/JavaScript library to read/change original xbox eeprom files.

This is a fairly niché piece of software considering there are readily available desktop and Xbox applications that do this exact thing.

The library requires no additional dependencies.

## Building and contributing

* Prerequisites:`node` and `npm`.
* To build: `npm run build`
* To run linter: `npm run lint`
* To run tests: `npm run test`

Any contributions are welcome. 

## Changelog

### v0.0.1

Initial version that can decrypt an EEPROM and generate a harddrive password. 
